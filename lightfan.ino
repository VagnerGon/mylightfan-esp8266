#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

const char *ssid = "Rede Gon"; //  SSID da sua rede
const char *password = "bazinga!"; // senha da sua rede

IPAddress ip(192,168,9,56); // ip  atribuido ao módulo
IPAddress gateway(192,168,31,1); // base ip da sua rede
IPAddress subnet(255,255,255,0);

/*converte os pinos Tx&Rx em entrada
 De forma a usar os pinos TX e RX como entrada para os pulsadores, 
 Deve habilitar o " #define habilita_pulsadores true" de forma a fazer com que estes pinos virem duas entradas.
 Atente que após isso realizado estes dois pinos IO1 e IO3 perdem a função de Serial Tx/Rx no entando em nada interfere no modo de atualização do módulo
 O que fazemos é tão  somente usar dois pinos que estavam perdidos como duas I/O
 Caso precise fazer algum Debug em sua aplicação, basta definir " habilita_pulsadores" como false - e após seus debug's mude novamente para true de forma a converte-los
 novamente para o modo I/O entrada
 */
#define habilita_pulsadores true

WiFiServer server(80); // porta para entrada externa

const int rele1 = 0;
const int rele2 = 2;
const int  pulsador = 1; //  usa o pino TX em entrada pulsador
const int  pulsador2 = 3;  // usa o pino RX em entrada pulsador

boolean estado_pulsador = HIGH;
boolean estado_pulsador2 = HIGH;

const char* APssid = "Placa do Gon"; // para mudar o nome da placa na rede
const char* APpassword = "bazingao"; // senha para conexão à placa

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 500;    // the debounce time; increase if the output flickers
int lastButtonState = LOW;  // the previous reading from the input pin
int lastButtonState2 = LOW; // the previous reading from the input pin

long timer1On = 0;
long timer1Off = 0;
long timer2On = 0;
long timer2Off = 0;

void createAPNetwork() {
  // Connect to WiFi network
  Serial.print("creating ");
  Serial.println(APssid);
  
  int channel = 2;
  WiFi.softAP(APssid, APpassword, channel);    
  WiFi.begin(ssid, password);
  WiFi.config( ip,  gateway,  subnet);

  delay(10);
}

void connectToNetwork () {
  Serial.println("Conectando a rede...");
  Serial.print("SSID: ");
  Serial.println(ssid);
  Serial.print("Senha: ");
  Serial.println(password);

  WiFi.config( ip,  gateway,  subnet);
  WiFi.begin( ssid, password );
  Serial.println();
  
  // Wait for connection
  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }
  Serial.println ();
  Serial.println("Conectado!");
}

void setup ( void ) {

  Serial.begin ( 115200 );
  
  pinMode ( rele1, OUTPUT );
  digitalWrite ( rele1, 1 );
  
  pinMode ( rele2, OUTPUT );
  digitalWrite ( rele2, 1 );

  //createAPNetwork();
  connectToNetwork();

  server.begin();
  Serial.println("Server Inicializado");
  Serial.print("Endereço IP: ");
  Serial.println(WiFi.localIP());

  if(habilita_pulsadores){
    delay(500);
    pinMode(pulsador,INPUT);
    pinMode(pulsador2,INPUT);
  }
}

String prepareHtmlPage() {
  return String("HTTP/1.1 200 OK") +
        "Content-Type: text/html\r\n" +
        "Connection: close\r\n" + 
        "\r\n" +
        "<!DOCTYPE HTML>" +
        "<html>" +
        "<head>\r\n" +
        "<title>esp8266 webserver</title>\r\n" +
        "</head>\r\n" +
        "<body>\r\n" +
        "<h1><span style=\"color:#000000;\">ESP8266</span></h1>\r\n" +
        "<h3> ESP8266 Web Server</h3>" +
        "<p>RELE1 <a href=\"?function=Rele1_on\"><button>ON</button></a> <a href=\"?function=Rele1_off\"><button>OFF</button></a></p>" +
        "<p>RELE2 <a href=\"?function=Rele2_on\"><button>ON</button></a> <a href=\"?function=Rele2_off\"><button>OFF</button></a></p>" +
        "</html>" +
        "\n";
}

void sendCurrentStatus(WiFiClient client) {
  const size_t capacity = 2*JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(4);
  DynamicJsonDocument doc(capacity);

  doc["lightOn"] = digitalRead(rele1);
  doc["fanOn"] = digitalRead(rele2);
  
  JsonObject lightTimer = doc.createNestedObject("lightTimer");
  lightTimer["on"] = timer1On;
  lightTimer["off"] = timer1Off;

  JsonObject fanTimer = doc.createNestedObject("fanTimer");
  fanTimer["on"] = timer2On;
  fanTimer["off"] = timer2Off;

  client.println("HTTP/1.0 200 OK");
  client.println("Content-Type: application/json");
  client.println();
  serializeJsonPretty(doc, client);
}

long checkTimer (String response, String key, long defaut) {
  int startIndex = response.indexOf(key);
  if (startIndex < 0) 
    return defaut;
  startIndex += key.length();
  String timeValue = response.substring(startIndex, response.indexOf("$", startIndex));
  if (timeValue.length() == 0)
    return defaut;
  return millis() + timeValue.toInt();
}

void readResponse(String response) {
  if (response.indexOf("Rele1_on") >= 0)
    digitalWrite(rele1, 0);
  if (response.indexOf("Rele1_off") >= 0)
    digitalWrite(rele1, 1);
  if (response.indexOf("Rele2_on") >= 0)
    digitalWrite(rele2, 0);
  if (response.indexOf("Rele2_off") >= 0)
    digitalWrite(rele2, 1);
  timer1On = checkTimer(response, "Timer1ON=", timer1On);
  timer1Off = checkTimer(response, "Timer1OFF=", timer1Off);
  timer2On = checkTimer(response, "Timer2ON=", timer2On);
  timer2Off = checkTimer(response, "Timer2OFF=", timer2Off);
}

void updateStateBySwitchChanges() {
  int estado_pulsador = digitalRead(pulsador);
  int estado_pulsador2 = digitalRead(pulsador2);
  
  if (estado_pulsador != lastButtonState)
    digitalWrite(rele1,!digitalRead(rele1));

  if (estado_pulsador2 != lastButtonState2)
    digitalWrite(rele2,!digitalRead(rele2));
  
  lastButtonState = estado_pulsador;
  lastButtonState2 = estado_pulsador2;
}

void closeClient(WiFiClient client) {
  delay(1);
  client.stop();
}

void checkTimers () {
  long elapsedTime = millis();
  if (timer1On > 0 && elapsedTime > timer1On) {
    digitalWrite(rele1, 0);
    timer1On = 0;
  }
  if (timer1Off > 0 && elapsedTime > timer1Off) {
    digitalWrite(rele1, 1);
    timer1Off = 0;
  }
  if (timer2On > 0 && elapsedTime > timer2On) {
    digitalWrite(rele2, 0);
    timer2On = 0;
  }
  if (timer2Off > 0 && elapsedTime > timer2Off) {
    digitalWrite(rele2, 1);
    timer2Off = 0;
  }
}

void loop ( void ) {
    
  updateStateBySwitchChanges();
  checkTimers();

  // Listenning for new clients
  WiFiClient client = server.available();
  if (!client) {
    closeClient(client);
    return;
  }
  
  Serial.println("Novo Cliente");

  String response;
  boolean blank_line = true;
  while (client.connected()) {
    if (client.available()) {
      char c = client.read();
      response += c;
      if (c == '\n' && blank_line) {
        sendCurrentStatus(client);
        readResponse(response);
        response = "";
        break;
      }
      if (c == '\n')
        blank_line = true;
      else if (c != '\r')
        blank_line = false;
    } else 
      delay(10);
  }  
  closeClient(client);
  Serial.println("Cliente desconectado.");
}
